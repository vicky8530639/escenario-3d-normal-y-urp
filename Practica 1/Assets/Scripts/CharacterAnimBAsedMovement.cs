using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]

public class CharacterAnimBAsedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;
    public int degreesToTurn = 160;

    [Header("Animator Parameters")]

    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
    public string turn180Param = "turn180";

    [Header("Animation Smoothing")]
    
    [Range(0, 1f)]
    
    public float StartAnimTime = 0.3f;
    
    [Range(0, 1f)]

    public float StopAnimTime = 0.15f;

    private float Speed;
    private Vector3 desireMoveDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;


    public float idleAnimationInterval = 6f;

    private float idleTypeSmooth = 2f; // Variable para suavizar la transici�n

    private bool isTouchingWall = false; // Variable para controlar si el personaje est� tocando un muro


    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        // Aqu� determina el valor para el par�metro idleType
        float targetIdleType = Mathf.Floor(Time.time * (1 / idleAnimationInterval)) % 3;

        idleTypeSmooth = Mathf.Lerp(idleTypeSmooth, targetIdleType, Time.deltaTime); // Suaviza la transici�n

        // Setea el valor del par�metro idleType en el Animator
        animator.SetFloat("idle_type", idleTypeSmooth);
    }

    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
    {
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;



        // Si est� tocando un muro, detener el movimiento
        if (isTouchingWall)
        {
            Speed = 0f;
        }
        else if (Speed >= Speed - rotationThreshold && dash)
        {
            Speed = 1.5f;
        }




        if (Speed >= Speed - rotationThreshold && dash)
        {
            Speed = 1.5f;
        }

        if(Speed > rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            desireMoveDirection = forward * vInput + right * hInput;

            if (Vector3.Angle(transform.forward,desireMoveDirection)>= degreesToTurn)
            {
                turn180 = true;
            }
            else
            {
                turn180 = false;
                transform.rotation = Quaternion.Slerp(transform.rotation,
                                                   Quaternion.LookRotation(desireMoveDirection),
                                                   rotationSpeed * Time.deltaTime);
            }

            animator.SetBool(turn180Param, turn180);

        }
        else if(Speed < rotationThreshold)
        {
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
        }
    }

    private void OnAnimatorIK(int layerIndex)
    {
        if (Speed < rotationThreshold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if(distanceToRightFoot > distanceToLeftFoot)
        {
            mirrorIdle = true;
        }
        else
        {
            mirrorIdle = false;
        }
    }

    // M�todo para activar/desactivar el estado de tocar el muro
    public void SetIsTouchingWall(bool value)
    {
        isTouchingWall = value;
    }
}
