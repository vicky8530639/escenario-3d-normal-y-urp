using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FToInspect : MonoBehaviour
{
    private Rigidbody rb;
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("F"))
        {
            anim.SetTrigger("Inspect");
        }
    }
    
}
