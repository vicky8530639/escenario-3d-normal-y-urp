using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Kill_Character : MonoBehaviour
{
    private Rigidbody rb;
    public Animator anim;
    public AudioSource audioSource;
    public AudioClip death;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        //anim = GetComponent<Animator>();

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Trap"))
        {
            Die();
        }
    }
    private void Die()
    {
        //rb.bodyType = RigidbodyType2D.Static;
        audioSource.PlayOneShot(death);
        anim.SetTrigger("Dead");
        Invoke("RestartLevel",4.3f);

    }

    private void RestartLevel()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
